import math
class Vector2D(object):
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y

    def toTuple(self):
        return (self.x, self.y)
    
    def rotate(self, origin, position, angle):
        ox, oy = origin.x, origin.y
        px, py = position.x, position.y

        qx = ox + math.cos(angle) * (px - ox) - math.sin(angle) * (py - oy)
        qy = oy + math.sin(angle) * (px - ox) + math.cos(angle) * (py - oy)
        
        return Vector2D(qx, qy)

    def magnitude(self):
        return math.sqrt(self.x**2 + self.y**2)

    def magnitudeSquared(self):
        return self.x**2 + self.y**2
    
    def normalize(self, vmag):
        if vmag == 0:
            normx = 0
            normy = 0
        else:
            normx = self.x / vmag
            normy = self.y / vmag
        return Vector2D(normx, normy)

    def __vecs(self):
        return (self.x, self.y)

    def __add__(self, rhs):
        return Vector2D(self.x + rhs.x, self.y + rhs.y)

    def __sub__(self, rhs):
        return Vector2D(self.x - rhs.x, self.y - rhs.y)

    def __neg__(self, rhs):
        return Vector2D(-self.x, -self.y)

    def __mul__(self, scalar):
        return Vector2D(self.x * scalar, self.y * scalar)

    def __div__(self, scalar):
        return Vector2D(self.x / scalar, self.y / scalar)
    
    def __hash__(self):
        return hash(self.__vecs())

    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        return False

    def copy(self):
        return Vector2D(self.x, self.y)