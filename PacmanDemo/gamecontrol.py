import os
os.environ['SDL_AUDIODRIVER'] = 'dsp'
import pygame
from pygame.locals import *
from constants import *
from pacman import Pacman
from nodes import NodeGroup
from ghosts import Ghost
from pellets import PelletGroup
from ghosts import GhostGroup
from maze import Maze
from icons import Icons
from spritesheet import SpriteSheet
from text import TextGroup

class GameControl(object):
    def __init__(self, nn):
        pygame.init()
        pygame.display.set_caption('PacMan')

        self.screen = pygame.display.set_mode(SCREENSIZE, 0, 32)
        self.background = None
        self.setBackground()

        self.clock = pygame.time.Clock()
        self.time = 0
        self.total_sec = 0
        self.frame_rate = 60
        self.level = 0
        self.lives = LIVES
        self.idleTimer = 0
        self.activeTimer = 0
        self.displayedLevel = 0
        self.maxLevels = 2
        self.ghostScore = 5
        
        self.score = 0
        self.internalScore = 0
        self.ttime = 0 #elapsed time for player
        self.neural_net = nn

        self.sheet = SpriteSheet()
        self.gameIcon = Icons(self.sheet)
        self.maze = Maze(self.level, self.sheet)
        self.maze.fillMaze(self.background)
        self.allText = TextGroup()
        self.allText.add("score_label", "SCORE", y=HEIGHT, align="left")
        self.allText.add("score", self.score, x=NCOLS*3.5, y=HEIGHT, align="defx")
        self.allText.add("time_label", "TIME", x=NCOLS*10, y=HEIGHT, align="defx")
        
    
    def setBackground(self):
        self.background = pygame.surface.Surface(SCREENSIZE).convert()
        self.background.fill(BLACK)

    def startGame(self):
        self.nodes = NodeGroup(self.level)
        self.pellets = PelletGroup(self.level)
        self.score = 0
        self.internalScore = 0
        self.total_sec = 0
        self.commonSetup()
    
    def restartLevel(self):
        self.commonSetup()
    
    def commonSetup(self):
        self.pacman = Pacman(self.nodes, self.level, self.sheet, self.neural_net)
        self.ghosts = GhostGroup(self.nodes, self.level, self.sheet)
        self.paused = True
        self.pauseTimer = 0
        self.pauseTime = 0
        self.playerPaused = True
        self.startDelay = False
        self.restartDelay = False
        self.scoreAccumulator = 0
        self.maxLives = MAXLIVES
        self.allText.add("ready_label", "READY!", y=20*HEIGHT, align="center", color=GREEN)
    
    def play(self):
        self.startGame()
        while True:
            self.update()
            if not self.pacman.alive:
                pass

    def applyScore(self):
        if self.scoreAccumulator > 0:
            newScore = self.score + self.scoreAccumulator
            if ((newScore % 10000 - self.score % 10000) < 0 or newScore - self.score >= 10000):
                if self.lives < self.maxLives:
                    self.lives += 1
            self.score += self.scoreAccumulator
            self.scoreAccumulator = 0
            self.allText.remove("score")
            self.score = newScore
            self.allText.add("score", self.score, x=NCOLS*3.5, y=HEIGHT, align="defx")
    
    def updateTime(self, dt):
        self.total_sec += dt
        minutes = round(self.total_sec) // 60
        seconds = round(self.total_sec) % 60
        output_string = "{0:02}:{1:02}".format(minutes, seconds)
        self.allText.remove("time")
        self.allText.add("time", output_string, x=NCOLS*13, y=HEIGHT, align="defx")
        return round(self.total_sec)
    
    def checkEvents(self):
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                exit()
            if event.type == KEYDOWN:
                if event.key == K_SPACE:
                    if self.paused:
                        self.playerPaused = False
                        self.allText.remove("paused_label")
                    else:
                        self.playerPaused = True
                        self.allText.add("paused_label", "PAUSED", y=20*HEIGHT, align="center", color=GREEN)
                    self.paused = not self.paused
                if event.key == K_k:
                    self.kill()

                if event.key ==K_r:
                    self.play()

                if event.key == K_ESCAPE:
                    pygame.quit()
                    exit()
    
    def checkPelletEvents(self, dt):
        pellet = self.pacman.eatPellets(self.pellets.pelletList)
        if pellet:
            self.idleTimer = 0
            self.scoreAccumulator += pellet.value
            self.pellets.numEaten += 1
            self.pellets.pelletList.remove(pellet)
            if self.ghosts.anyInFrightOrSpawn():
                self.pacman.boostSpeed()
            else:
                self.pacman.reduceSpeed()
            if pellet.name == "powerpellet":
                self.ghostScore = 5
                self.ghosts.setFrightMode()
            if self.ghosts.anyInFrightOrSpawn():
                self.pacman.boostSpeed()
            if self.pellets.isEmpty():
                self.paused = True
                self.pauseTime = 3
                self.pauseTimer = 0
                self.startDelay = True
                print(" MAZE COMPLETE")
        else:
            self.idleTimer += dt
            if self.ghosts.anyInFrightOrSpawn():
                self.pacman.boostSpeed()
            else:
                if self.idleTimer >= 0.5:
                    self.pacman.normalSpeed()
    
    def checkGhostEvents(self, dt):
        ghost = self.pacman.eatGhost(self.ghosts)
        if ghost is not None:
            if ghost.mode.name == "FRIGHT":
                ghost.setRespawnMode()
                self.scoreAccumulator += self.ghostScore
                self.ghostScore *= 2
                self.paused = True
                self.pauseTime = 0.1
                self.pauseTimer = 0
            elif ghost.mode.name != "SPAWN":
                self.lives -= 1
                self.paused = True
                self.pauseTime = 0.1
                self.pauseTimer = 0
                self.pacman.alive = False
                self.pacman.animate.setAnimation("death", 0)
                if self.lives == 0:
                    self.lives = LIVES
                    self.startDelay = True
                else:
                    self.restartDelay = True
                
        if self.pellets.numEaten >= 30 or self.idleTimer >= 10:
            self.ghosts.release("inky")
        if self.pellets.numEaten >= 60 or self.idleTimer >= 10:
            self.ghosts.release("clyde")
    
    def kill(self):
        self.lives -= 1
        self.paused = True
        self.pauseTime = 0.1
        self.pauseTimer = 0
        self.pacman.alive = False
        if self.lives == 0:
            self.lives = LIVES
            self.startDelay = True
    
    def killTimer(self): #used during training; kills pacman if he's been idle for too long
        idleTime = round(self.idleTimer)
        if self.pacman.direction == STOP and idleTime != 0:
            if self.score < 100 and idleTime == 5:
                self.kill()
        
        if self.score < 100 and idleTime == 3:
            self.kill()

        if idleTime == 40:
            self.kill()
    
    def active(self, dt):
        idleTime = round(self.idleTimer)
        if self.pacman.direction != STOP and idleTime == 0:
            self.activeTimer += dt
            self.internalScore += dt
        else:
            self.activeTimer = 0

    def update(self):
        self.time += 1
        dt = self.clock.tick(self.frame_rate) / 1000.0
        self.allText.remove("GameOver_label")
        if not self.paused:
            self.allText.remove("ready_label")
            self.pacman.update(dt, self.ghosts, self.pellets.pelletList)
            self.ghosts.update(dt, self.pacman)
            self.active(dt)
            self.ttime = self.updateTime(dt)
            self.checkPelletEvents(dt)
            self.checkGhostEvents(dt)
            #self.killTimer()
        else:
            if not self.playerPaused:
                if not self.pacman.alive:
                    self.allText.add("GameOver_label", "Game Over!", y=20*HEIGHT, align="center", color=RED)
                    self.pacman.update(dt, self.ghosts, self.pellets.pelletList)
                    if self.pacman.deathSequenceFinished:
                        if self.startDelay == True:
                            self.play()
                        if self.restartDelay == True:
                            self.restartLevel()
                else:
                    self.pauseTimer += dt
                    if self.pauseTimer >= self.pauseTime:
                        self.paused = False
                    if self.startDelay == True:
                        self.startGame()
                    if self.restartDelay == True:
                        self.restartLevel()
        self.checkEvents()
        self.applyScore()
        self.render()
    
    def render(self):
        self.screen.blit(self.background, (0, 0))
        #self.nodes.render(self.screen)
        self.pellets.render(self.screen)
        self.pacman.render(self.screen)
        self.ghosts.render(self.screen)
        self.allText.render(self.screen)
        pygame.display.update()

""" if __name__ == "__main__":
    game = GameControl()
    game.startGame()
    while True:
        game.update() """