import pygame
import random
from entities import MazeEntity
from constants import *
from stacks import Stack
from vectors import Vector2D

class Mode(object):
    def __init__(self, name="", time=None, speedMult=1):
        self.name = name
        self.time = time
        self.speedMult = speedMult

class Ghost(MazeEntity):
    def __init__(self, nodes, level, spritesheet):
        MazeEntity.__init__(self, nodes, level, spritesheet)
        self.name = "ghost"
        self.goal = Vector2D()
        self.modeStack = self.setupModeStack()
        self.mode = self.modeStack.pop()
        self.modeTimer = 0
        self.homeNode = None
        self.startDirection = UP
        self.exitHome = True
        self.guideOut = False
        self.leftHome = True
        self.previousDirection = Vector2D()

    def setStartPosition(self):
        self.setHomeNode()
        self.direction = self.startDirection
        self.target = self.node.neighbors[self.direction]
        self.setPosition()
        self.checkDirectionChange()
    
    def getStartNode(self):
        node = MAZEDATA[self.level]["start"]["ghosts"]
        return self.nodes.getNode(*node, nodeList=self.nodes.homeList)
    
    def getValidDirections(self):
        validDirections = []
        for key in self.node.neighbors.keys():
            if self.node.neighbors[key] is not None:
                if not key == self.direction * -1:
                    validDirections.append(key)

        if len(validDirections) == 0:
            validDirections.append(self.forceBacktrack())
        if (self.node.home and DOWN in validDirections and self.mode.name != "SPAWN"):
            validDirections.remove(DOWN)
        if self.node.goUP and UP in validDirections:
            validDirections.remove(UP)
        if not self.leftHome:
            if self.exitHome:
                validDirections = self.guideOutOfHome(validDirections)
            else:
                validDirections = self.trapInHome(validDirections)

        return validDirections
    
    def trapInHome(self, validDirections):
        if LEFT in validDirections:
            validDirections.remove(LEFT)
        if RIGHT in validDirections:
            validDirections.remove(RIGHT)
        return validDirections
    
    def guideOutOfHome(self, validDirections):
        if not self.guideOut:
            if self.target == self.homeNode:
                self.guideOut = True
                validDirections = []
                validDirections.append(self.guideStack.pop())
        else:
            validDirections = []
            validDirections.append(self.guideStack.pop())
            if self.guideStack.isEmpty():
                self.guideOut = False
                self.leftHome = True
        return validDirections
    
    def setGuideStack(self):
        self.guideStack = Stack()
        self.guideStack.push(LEFT)
        self.guideStack.push(UP)
    
    def getClosestDirection(self, validDirections):
        distances = []
        for direction in validDirections:
            diffVec = self.node.position + direction*WIDTH - self.goal
            distances.append(diffVec.magnitudeSquared())
        index = distances.index(min(distances))
        return validDirections[index]
    
    def randomDirection(self, validDirections):
        index = randint(0, len(validDirections) - 1)
        return validDirections[index]
    
    def moveBySelf(self):
        if self.overshotTarget():
            self.node = self.target
            self.portal()
            validDirections = self.getValidDirections()
            self.direction = self.getClosestDirection(validDirections)
            self.target = self.node.neighbors[self.direction]
            self.setPosition()
            if self.mode.name == "SPAWN":
                if self.position == self.goal:
                    self.mode = self.modeStack.pop()
    
    def forceBacktrack(self):
        if self.direction * -1 == UP:
            return UP
        if self.direction * -1 == DOWN:
            return DOWN
        if self.direction * -1 == LEFT:
            return LEFT
        if self.direction * -1 == RIGHT:
            return RIGHT
    
    def setupModeStack(self):
        modes = Stack()
        modes.push(Mode(name="CHASE"))
        modes.push(Mode(name="SCATTER", time=SCATTERTIME))
        modes.push(Mode(name="CHASE", time=CHASETIME))
        modes.push(Mode(name="SCATTER", time=SCATTERTIME))
        modes.push(Mode(name="CHASE", time=CHASETIME))
        modes.push(Mode(name="SCATTER", time=SCATTERTIME))
        modes.push(Mode(name="CHASE", time=CHASETIME))
        modes.push(Mode(name="SCATTER", time=SCATTERTIME))
        return modes
    
    def modeUpdate(self, dt):
        self.modeTimer += dt
        if self.mode.time is not None:
            if self.modeTimer >= self.mode.time:
                self.reverseGhostDirection()
                self.mode = self.modeStack.pop()
                self.modeTimer = 0
    
    def setFrightMode(self):
        if self.mode.name != "FRIGHT" and self.mode.name != "SPAWN":
            if self.mode.time is not None:
                    dt = self.mode.time - self.modeTimer
                    self.modeStack.push(Mode(name=self.mode.name, time=dt))
            else:
                self.modeStack.push(Mode(name=self.mode.name))
            self.reverseGhostDirection()
            self.mode = Mode("FRIGHT", time=FRIGHTTIME, speedMult=0.5)
            self.modeTimer = 0
        elif self.mode.name != "SPAWN":
            self.mode = Mode("FRIGHT", time=FRIGHTTIME, speedMult=0.5)
            self.modeTimer = 0
    
    def setRespawnMode(self):
        self.mode = Mode("SPAWN", speedMult=2)
        self.modeTimer = 0
        self.setGuideStack()
        self.leftHome = False
    
    def setScatterGoal(self):
        self.goal = Vector2D()

    def setChaseGoal(self, pacman):
        self.goal = pacman.position

    def setRandomGoal(self):
        random.seed(6000)
        x = random.randint(0, NCOLS*WIDTH)
        y = random.randint(0, NROWS*HEIGHT)
        self.goal = Vector2D(x, y)
    
    def setSpawnGoal(self):
        self.goal = self.homeNode.position
    
    def update(self, dt, pacman, blinky):
        speedMod = self.modifySpeed()
        self.position += self.direction*speedMod*dt
        self.modeUpdate(dt)
        if self.mode.name == "CHASE":
            self.setChaseGoal(pacman, blinky)
            self.checkDirectionChange()
        elif self.mode.name == "SCATTER":
            self.setScatterGoal()
            self.checkDirectionChange()
        elif self.mode.name == "FRIGHT":
            self.setRandomGoal()
            self.setImage(6, 0)
            self.checkDirectionChange()
        elif self.mode.name == "SPAWN":
            self.setSpawnGoal()
            self.setImage(6, 4)
            self.checkDirectionChange()
        self.moveBySelf()
    
    def checkDirectionChange(self):
        if self.direction != self.previousDirection:
            self.previousDirection = self.direction
            row = self.imageRow
            col = 0
            if self.mode.name != "FRIGHT":
                if self.direction == LEFT:
                    col = 4
                elif self.direction == RIGHT:
                    col = 6
                elif self.direction == DOWN:
                    col = 2
                elif self.direction == UP:
                    col = 0
            self.setImage(row, col)
    
    def setImage(self, row, col):
        self.image = self.spritesheet.getImage(col, row, 32, 32)
    
    def setSpawnImages(self):
        row = 6
        if self.direction == LEFT:
            col = 4
        elif self.direction == RIGHT:
            col = 6
        elif self.direction == DOWN:
            col = 2
        elif self.direction == UP:
            col = 0
        return row, col
    
    def reverseGhostDirection(self):
        if self.leftHome:
            self.reverseDirection()

    def modifySpeed(self):
        if (self.node.portalNode is not None or self.target.portalNode is not None):
            return self.speed / 2.0
        return self.speed * self.mode.speedMult

class Blinky(Ghost):
    def __init__(self, nodes, level, spritesheet):
        Ghost.__init__(self, nodes, level, spritesheet)
        self.name = "blinky"
        self.color = RED
        self.startDirection = LEFT
        self.imageRow = 2
        self.setStartPosition()

    def setScatterGoal(self):
        self.goal = Vector2D(WIDTH * (NCOLS-6), 0)

    def setChaseGoal(self, pacman, blinky=None):
        self.goal = pacman.position

    def setHomeNode(self):
        node = self.getStartNode()
        self.homeNode = node
        self.node = self.homeNode.neighbors[UP]

class Pinky(Ghost):
    def __init__(self, nodes, level, spritesheet):
        Ghost.__init__(self, nodes, level, spritesheet)
        self.name = "pinky"
        self.color = PINK
        self.imageRow = 3
        self.setStartPosition()

    def setScatterGoal(self):
        self.goal = Vector2D()

    def setChaseGoal(self, pacman, blinky=None):
        self.goal = pacman.position + pacman.direction * WIDTH * 4
    
    def setHomeNode(self):
        node = self.getStartNode()
        self.homeNode = node
        self.node = node

class Inky(Ghost):
    def __init__(self, nodes, level, spritesheet):
        Ghost.__init__(self, nodes, level, spritesheet)
        self.name = "inky"
        self.color = TEAL
        self.startDirection = DOWN
        self.imageRow = 4
        self.setStartPosition()
        self.exitHome = False
        self.setGuideStack()
        self.leftHome = False

    def setScatterGoal(self):
        self.goal = Vector2D(WIDTH*NCOLS, HEIGHT*NROWS)
    
    def setChaseGoal(self, pacman, blinky=None):
        vec1 = pacman.position + pacman.direction * WIDTH * 2
        vec2 = (vec1 - blinky.position) * 2
        self.goal = blinky.position + vec2
    
    def setHomeNode(self):
        node = self.getStartNode()
        self.homeNode = node.neighbors[LEFT]
        self.node = node.neighbors[LEFT]
    
    def setGuideStack(self):
        self.guideStack = Stack()
        self.guideStack.push(LEFT)
        self.guideStack.push(UP)
        self.guideStack.push(RIGHT)

class Clyde(Ghost):
    def __init__(self, nodes, level, spritesheet):
        Ghost.__init__(self, nodes, level, spritesheet)
        self.name = "clyde"
        self.color = ORANGE
        self.startDirection = DOWN
        self.imageRow = 5
        self.setStartPosition()
        self.exitHome = False
        self.setGuideStack()
        self.leftHome = False

    def setScatterGoal(self):
        self.goal = Vector2D(0, HEIGHT*NROWS)
    
    def setChaseGoal(self, pacman, blinky=None):
        d = pacman.position - self.position
        ds = d.magnitudeSquared()
        if ds <= (WIDTH * 8)**2:
            self.setScatterGoal()
        else:
            self.goal = pacman.position
    
    def setHomeNode(self):
        node = self.getStartNode()
        self.homeNode = node.neighbors[RIGHT]
        self.node = node.neighbors[RIGHT]
    
    def setGuideStack(self):
        self.guideStack = Stack()
        self.guideStack.push(LEFT)
        self.guideStack.push(UP)
        self.guideStack.push(LEFT)

class GhostGroup(object):
    def __init__(self, nodes, level, spritesheet):
        self.nodes = nodes
        self.level = level
        self.ghosts = []
        self.ghosts.append(Blinky(nodes, level, spritesheet))
        self.ghosts.append(Pinky(nodes, level, spritesheet))
        self.ghosts.append(Inky(nodes, level, spritesheet))
        self.ghosts.append(Clyde(nodes, level, spritesheet))

    def __iter__(self):
        return iter(self.ghosts)
    
    def __getitem__(self, index):
        return self.ghosts[index]

    def update(self, dt, pacman):
        blinky = self.getGhost("blinky")
        for ghost in self:
            ghost.update(dt, pacman, blinky)

    def setFrightMode(self):
        for ghost in self:
            ghost.setFrightMode()

    def anyInFright(self):
        for ghost in self:
            if ghost.mode.name == "FRIGHT":
                return True
            return False

    def anyInSpawn(self):
        for ghost in self:
            if ghost.mode.name == "SPAWN":
                return True
        return False

    def anyInFrightOrSpawn(self):
        for ghost in self:
            if ghost.mode.name == "FRIGHT" or ghost.mode.name == "SPAWN":
                return True
        return False

    def getGhost(self, name):
        for ghost in self:
            if ghost.name == name:
                return ghost
        return None
    
    def release(self, name):
        ghost = self.getGhost(name)
        if ghost is not None:
            ghost.exitHome = True

    def render(self, screen):
        for ghost in self:
            ghost.render(screen)