import neat
import pickle
import sys
from gamecontrol import GameControl


def run_winner(n=1):
    # Load configuration.
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         'config')
    
    #load Genome
    genome = pickle.load(open('winner-13891.pkl', 'rb')) #from 118th Generation (100 Generations since Ghosts)
    #genome = pickle.load(open('winner-208.pkl', 'rb')) #from 184th Generation (166 Generations since Ghosts)
    #genome = pickle.load(open('winner-bad.pkl', 'rb'))
    print("Genome ID: " + str(genome.key))
    print("Fitness: " + str(genome.fitness))
    nn = neat.nn.FeedForwardNetwork.create(genome, config)
    for i in range(0,n):
        # Play game and get results
        try:
            pacman_bio = GameControl(nn)
            pacman_bio.play()
        except KeyboardInterrupt:
            print("User break")
            break

def main():
    if len(sys.argv)>1:
        run_winner(int(sys.argv[1]))
    else:
        run_winner()

if __name__ == "__main__":
	main()