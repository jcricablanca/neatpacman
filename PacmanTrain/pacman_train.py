import neat
import pickle
import sys
import visualize

from gamecontrol import GameControl
#from gamecontrolng import GameControl #uncomment to use the maze without ghosts
from filereporter import FileReporter

fitlimit = 0
score_to_beat = 0
mostNumEaten = 0
longestTimeGlobal = 0
best_genome = 0
maze_completed_genomes = []
genNum = 118 #Start with this generation number when continuing from a checkpoint

def playGame(networks):
    scores = []

    for genome, net in networks:
        sys.stdout.write("\rGenome ID: %i" % genome.key)
        sys.stdout.flush()
        pacman_sim = GameControl(net)
        pacman_sim.play()
    
        score = {
            "score": pacman_sim.score,
            "internalScore": round(pacman_sim.internalScore)+pacman_sim.score,
            "numEaten": pacman_sim.pellets.numEaten,
            "time": pacman_sim.ttime,
            "network": pacman_sim.neural_net,
        }
        scores.append((genome, net, score))
    
    return scores

def test_genomes(net):
    pacman_sim = GameControl(net)
    pacman_sim.play()

    score = {
        "score": pacman_sim.score,
        "internalScore": round(pacman_sim.internalScore)+pacman_sim.score,
        "numEaten": pacman_sim.pellets.numEaten,
        "time": pacman_sim.ttime,
    }
    
    return score

def evaluate_genomes(genomes, config):
    networks = []

    for gid, g in genomes:
        networks.append((g, neat.nn.FeedForwardNetwork.create(g, config)))
    
    scores = playGame(networks)
    
    global genNum
    global maze_completed_genomes
    top_score = 0
    longest_time = 0
    top_internalScore = 0
    top_numEaten = 0
    top_genome = 0

    for genome, net, score in scores:
        global fitlimit
        global score_to_beat
        global mostNumEaten
        global longestTimeGlobal
        global best_genome
        gamescore = score["score"]
        internalScore = score["internalScore"]
        time = score["time"]
        numEaten = score["numEaten"]
        
        fitness = internalScore**2

        if gamescore > score_to_beat and fitlimit < 200000: 
            fitlimit = fitlimit + 0.1
        
        genome.fitness = -1 if fitness <= fitlimit else fitness

        if top_score < gamescore:
            top_score = gamescore
        
        if score_to_beat < top_score:
            score_to_beat = top_score
        
        if top_internalScore < internalScore:
            top_internalScore = internalScore

        if longest_time < time:
            longest_time = time
        
        if longestTimeGlobal < time:
            longestTimeGlobal = time
        
        if mostNumEaten < numEaten:
            best_genome = genome.key
            mostNumEaten = numEaten

        if top_numEaten < numEaten:
            top_genome = genome.key
            top_numEaten = numEaten
        
        if numEaten == 244:
            maze_completed_genomes.append(genome)

    print("\n-----------------------------")
    print('Best Genome: ', best_genome)
    print('Highest score: ', score_to_beat)
    print('Most no. of pellets eaten: ', mostNumEaten)
    print('Longest time: ', longestTimeGlobal)
    print('--This Generation Stats--')
    print('Top Genome: ', top_genome)
    print('TopNumEaten: ', top_numEaten)
    print('The top score was: ', top_score)
    print('Top Internal Score: ', top_internalScore)
    print('The longest time was: ', longest_time)
    print("-----------------------------")
    with open('statistics/Statistics.txt', "a+") as ff:
        ff.write('*** Generation {0} ***\n'.format(genNum))
        ff.write('Highest score: {0}\n'.format(score_to_beat))
        ff.write('Most no. of pellets eaten: {0}\n'.format(mostNumEaten))
        ff.write('Best Genome: {0}\n'.format(best_genome))
        ff.write('Longest time: {0}\n'.format(longestTimeGlobal))
        ff.write('--This Generation Stats--\n')
        ff.write('TopNumEaten: {0}\n'.format(top_numEaten))
        ff.write('Top Genome: {0}\n'.format(top_genome))
        ff.write('The top score was: {0}\n'.format(top_score))
        ff.write('Top Internal Score: {0}\n'.format(top_internalScore))
        ff.write('The longest time was: {0}\n'.format(longest_time))
        ff.write("-----------------------------\n")
    genNum += 1

def run(n=100):
    config = neat.Config(neat.DefaultGenome, neat.DefaultReproduction,
                         neat.DefaultSpeciesSet, neat.DefaultStagnation,
                         'config')
    
    location = "statistics/"
    filename = "generation-gp"

    # Create the population, which is the top-level object for a NEAT run.
    p = neat.Population(config)
    #p = neat.Checkpointer.restore_checkpoint('neat-checkpoint-118')
    stats = neat.StatisticsReporter()
    p.add_reporter(stats)
    # Add a stdout reporter to show progress in the terminal.
    p.add_reporter(neat.StdOutReporter(True))
    p.add_reporter(FileReporter(True, location, filename))
    p.add_reporter(neat.Checkpointer(1)) #Checkpoint every 1 generation.

    global maze_completed_genomes
    node_names = {
        -1:"up",
        -2:"down",
        -3:"left",
        -4:"right",
        -5:"uppellet",
        -6:"downpellet",
        -7:"leftpellet",
        -8:"rightpellet",
        -9:"pisup",
        -10:"pisdown",
        -11:"pisleft",
        -12:"pisright",
        0:"UP",
        1:"DOWN",
        2:"LEFT",
        3:"RIGHT"
    }
    while 1:
        try:
            winner = p.run(evaluate_genomes, n=n)

            best_gen = maze_completed_genomes
            solved = True
            best_scores = []
            
            print("Maze completed: " + str(len(maze_completed_genomes)) + " times.")
            print("Testing Genomes...")
            for g in best_gen:
                print("Genome ID: " + str(g.key) + " Fitness: " + str(g.fitness))
                nn = neat.nn.FeedForwardNetwork.create(g, config)
                score = test_genomes(nn)
                numEaten = score["numEaten"]
                best_scores.append(numEaten)
                if numEaten == 244:
                    name = 'winners/winner-{0}'.format(g.key)
                    with open(name+'.pkl', 'wb') as f:
                        pickle.dump(g, f)
                    visualize.draw_net(config, g, view=False, filename=name+'-outputnet', node_names=node_names)
                    visualize.plot_stats(stats, ylog=False, view=False, filename=name+'-avg_fitness.svg')
                    visualize.plot_species(stats, filename=name+'-speciation.svg', view=False)
            
            #Get average score of successful genomes
            if len(best_scores) > 0:
                avg_score = sum(best_scores) / len(best_scores)
            else:
                avg_score = 0
            
            if avg_score < 244:
                print("Avg NumEaten: " + str(avg_score))
                solved = False
                maze_completed_genomes.clear()
            print("Best scores: " + str(len(best_scores))) 
            print(best_scores)
            
            if solved:
                print("Solved")
                print("Avg NumEaten: " + str(avg_score))

                for n, g in enumerate(best_gen):
                    name = 'winners/winner244-{0}'.format(g.key)
                    with open(name+'.pkl', 'wb') as f:
                        pickle.dump(g, f)
                    visualize.draw_net(config, g, view=False, filename=name+'outputnet', node_names=node_names)
                    visualize.plot_stats(stats, ylog=False, view=False, filename=name+'-avg_fitness.svg')
                    visualize.plot_species(stats, filename=name+'-speciation.svg', view=False)
                
                break
        
        except KeyboardInterrupt:
            print("User break")
            break

def main():
    if len(sys.argv)>1:
        run(int(sys.argv[1]))
    else:
        run()

if __name__ == "__main__":
	main()