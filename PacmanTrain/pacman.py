import pygame
import neat
import math
import numpy as np
import random
from pygame.locals import *
from vectors import Vector2D
from constants import *
from entities import MazeEntity

class Pacman(MazeEntity):
    def __init__(self, nodes, level, nn):
        MazeEntity.__init__(self, nodes, level)
        #Pacman NN Feedforward Brain
        self.neural_net = nn
        
        self.name = "pacman"
        self.color = YELLOW
        self.r = 4 
        self.setStartPosition()
        self.alive = True
        self.directions = [UP, DOWN, LEFT, RIGHT]
    
    def setStartPosition(self):
        self.direction = LEFT
        pos = MAZEDATA[self.level]["start"]["pacman"]
        self.node = self.nodes.getNode(*pos, nodeList=self.nodes.nodeList)
        self.target = self.node.neighbors[self.direction]
        self.setPosition()
        halfway = (self.node.position.x - self.target.position.x) / 2
        self.position.x -= halfway
    
    def moveDecision(self, ghosts, pelletList):
        pellet = self.getNearestPellet(pelletList)
        validDirections = self.getValidDirections()
        validdirs = self.validDirs()
        up = 0
        down = 0
        left = 0
        right = 0
        uppellet = 0
        downpellet = 0
        leftpellet = 0
        rightpellet = 0
        pisup = 0
        pisdown = 0
        pisleft = 0
        pisright = 0

        
        if UP in validdirs:
            if self.ghostInDirection(ghosts, UP) is not None:
                up = self.ghostInDirection(ghosts, UP)
            elif self.ghostInDirectionAdv(ghosts, UP) is not None:
                up = self.ghostInDirectionAdv(ghosts, UP)
            
            if self.pelletInDirection(pelletList, UP) is not None:
                uppellet = self.pelletInDirection(pelletList, UP) + 1
        
        if DOWN in validdirs:
            if self.ghostInDirection(ghosts, DOWN) is not None:
                down = self.ghostInDirection(ghosts, DOWN)
            elif self.ghostInDirectionAdv(ghosts, DOWN) is not None:
                down = self.ghostInDirectionAdv(ghosts, DOWN)
            
            if self.pelletInDirection(pelletList, DOWN) is not None:
                downpellet = self.pelletInDirection(pelletList, DOWN)
        
        if LEFT in validdirs:
            if self.ghostInDirection(ghosts, LEFT) is not None:
                left = self.ghostInDirection(ghosts, LEFT)
            elif self.ghostInDirectionAdv(ghosts, LEFT) is not None:
                left = self.ghostInDirectionAdv(ghosts, LEFT)
            
            if self.pelletInDirection(pelletList, LEFT) is not None:
                leftpellet = self.pelletInDirection(pelletList, LEFT) + 1
        
        if RIGHT in validdirs:
            if self.ghostInDirection(ghosts, RIGHT) is not None:
                right = self.ghostInDirection(ghosts, RIGHT)
            elif self.ghostInDirectionAdv(ghosts, RIGHT) is not None:
                right = self.ghostInDirectionAdv(ghosts, RIGHT)

            if self.pelletInDirection(pelletList, RIGHT) is not None:
                rightpellet = self.pelletInDirection(pelletList, RIGHT)
        
        if self.getClosestDirection(validDirections, pellet) is UP:
            pisup = 1
        if self.getClosestDirection(validDirections, pellet) is DOWN:
            pisdown = 1
        if self.getClosestDirection(validDirections, pellet) is LEFT:
            pisleft = 1
        if self.getClosestDirection(validDirections, pellet) is RIGHT:
            pisright = 1


        #return self.getValidKey() #uncomment to enable directional key inputs
        
        input = (
            up,
            down,
            left,
            right,
            uppellet,
            downpellet,
            leftpellet,
            rightpellet,
            pisup,
            pisdown,
            pisleft,
            pisright,
        )
        
        output = self.neural_net.activate(input)
        if max(output) > 0.8:
            return self.directions[output.index(max(output))]

    def ghostInDirectionAdv(self, ghosts, direction):
        if ghosts is None:
            return None
        distanceToGhost = {}
        
        blinky = (ghosts[0].position + direction*HEIGHT) - self.target.position
        pinky = (ghosts[1].position + direction*HEIGHT) - self.target.position 
        inky = (ghosts[2].position + direction*HEIGHT) - self.target.position
        clyde = (ghosts[3].position + direction*HEIGHT) - self.target.position
        
        bnormal = blinky.normalize(blinky.magnitude())
        pnormal = pinky.normalize(pinky.magnitude())
        inormal = inky.normalize(inky.magnitude())
        cnormal = clyde.normalize(clyde.magnitude())

        if bnormal == direction:
            distanceToGhost[ghosts[0]] = blinky.magnitude()
        if pnormal == direction:
            distanceToGhost[ghosts[1]] = pinky.magnitude()
        if inormal == direction:
            distanceToGhost[ghosts[2]] = inky.magnitude()
        if cnormal == direction:
            distanceToGhost[ghosts[3]] = clyde.magnitude()

        if distanceToGhost:
            dist = 1/distanceToGhost[max(distanceToGhost, key=distanceToGhost.get)]
            if dist < 1:
                return dist

    def ghostInDirection(self, ghosts, direction):
        if ghosts is None:
            return None
        distanceToGhost = {}
        
        blinky = (ghosts[0].position + direction*HEIGHT) - self.position
        pinky = (ghosts[1].position + direction*HEIGHT) - self.position
        inky = (ghosts[2].position + direction*HEIGHT) - self.position
        clyde = (ghosts[3].position + direction*HEIGHT) - self.position
        
        bnormal = blinky.normalize(blinky.magnitude())
        pnormal = pinky.normalize(pinky.magnitude())
        inormal = inky.normalize(inky.magnitude())
        cnormal = clyde.normalize(clyde.magnitude())

        if bnormal == direction:
            distanceToGhost[ghosts[0]] = blinky.magnitude()
        if pnormal == direction:
            distanceToGhost[ghosts[1]] = pinky.magnitude()
        if inormal == direction:
            distanceToGhost[ghosts[2]] = inky.magnitude()
        if cnormal == direction:
            distanceToGhost[ghosts[3]] = clyde.magnitude()

        if distanceToGhost:
            dist = 1/distanceToGhost[max(distanceToGhost, key=distanceToGhost.get)]
            if dist < 1:
                return dist
    
    def pelletInDirection(self, pelletList, direction):
        if not pelletList:
            return None
        for pellet in pelletList:
            d = self.position+direction*HEIGHT - pellet.position
            dSquared = d.magnitudeSquared()
            rSquared = 4 * self.r**2
            if dSquared <= rSquared:
                return 1

    def getClosestDirection(self, validDirections, pellet):
        if pellet is None:
            return STOP
        distances = []
        
        for direction in validDirections:
            diffVec = self.position + direction - pellet.position
            distances.append(diffVec.magnitudeSquared())
        
        index = distances.index(np.min(distances))
        
        return validDirections[index]
    
    def getNearestPellet(self, pelletList):
        if not pelletList:
            return None
        distances = []
        for pellet in pelletList:
            diffVec = self.position - pellet.position
            distances.append(diffVec.magnitudeSquared())
        index = distances.index(np.min(distances))
        return pelletList[index]

    def getValidDirections(self): 
        validDirections = []
        for key in self.node.neighbors.keys():
            if self.node.neighbors[key] is not None:
                validDirections.append(key)
        
        return validDirections

    def validDirs(self):
        validDirs = []
        if self.node != self.target:
            for key in self.target.neighbors.keys():
                if self.target.neighbors[key] is not None:
                    validDirs.append(key)
        else:
            for key in self.node.neighbors.keys():
                if self.node.neighbors[key] is not None:
                    validDirs.append(key)
        
        return validDirs
    
    def getDistanceFrom(self, obj):
        d = self.position - obj.position
        droot = d.magnitude() #distance from obj
        return droot
    
    def update(self, dt, ghosts, pelletList):
        direction = self.moveDecision(ghosts, pelletList)
        self.position += self.direction*self.speed*dt
        if self.alive:
            if direction:
                self.moveByKey(direction)
            else:
                if self.direction == STOP:
                    self.moveDecision(ghosts, pelletList)
                self.moveBySelf(ghosts, pelletList)
            
            if self.overshotTarget():
                self.node = self.target
                self.setPosition()
                self.direction = STOP
    
    def getValidKey(self):
        key_pressed = pygame.key.get_pressed()
        if key_pressed[K_UP]:
            return UP
        if key_pressed[K_DOWN]:
            return DOWN
        if key_pressed[K_LEFT]:
            return LEFT
        if key_pressed[K_RIGHT]:
            return RIGHT
        else:
            return None
    
    def moveByKey(self, direction):
        if self.direction is STOP:
            if self.node.neighbors[direction] is not None:
                self.target = self.node.neighbors[direction]
                self.direction = direction
        else:
            if direction == self.direction * -1:
                self.reverseDirection()
            if self.overshotTarget():
                self.node = self.target
                self.portal()
                if self.node.neighbors[direction] is not None and not self.node.home:
                    self.target = self.node.neighbors[direction]
                    if self.direction != direction:
                        self.setPosition()
                        self.direction = direction
                else:
                    if self.node.neighbors[self.direction] is not None:
                        self.target = self.node.neighbors[self.direction]
                    else:
                        self.setPosition()
                        self.direction = STOP

    def moveBySelf(self, ghosts, pelletList):
        if self.direction is not STOP:
            if self.overshotTarget():
                self.node = self.target
                self.portal()
                if self.node.neighbors[self.direction] is not None:
                    self.moveDecision(ghosts, pelletList)
                    self.target = self.node.neighbors[self.direction]
                else:
                    self.moveDecision(ghosts, pelletList)
                    self.setPosition()
                    self.direction = STOP
    
    def eatObject(self, obj):
        d = self.position - obj.position
        dSquared = d.magnitudeSquared()
        rSquared = 4 * self.r**2
        if dSquared <= rSquared:
            return True
        return False
    
    def eatPellets(self, pelletList):
        for pellet in pelletList:
            if self.eatObject(pellet):
                return pellet
        return None
    
    def eatGhost(self, ghosts):
        for ghost in ghosts:
            if self.eatObject(ghost):
                return ghost
        return None
    
    def boostSpeed(self):
        self.speed = MAXSPEED * 1.5

    def normalSpeed(self):
        self.speed = MAXSPEED

    def reduceSpeed(self):
        self.speed = MAXSPEED