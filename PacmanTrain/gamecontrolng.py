import os
os.environ['SDL_AUDIODRIVER'] = 'dsp'
import pygame
from pygame.locals import *
from constants import *
from pacman import Pacman
from nodes import NodeGroup
from ghosts import Ghost
from pellets import PelletGroup
from ghosts import GhostGroup
from maze import Maze

class GameControl(object):
    def __init__(self, nn):
        pygame.init()

        self.screen = pygame.display.set_mode(SCREENSIZE, 0, 32)
        self.background = None
        self.setBackground()

        self.clock = pygame.time.Clock()
        self.frame_count = 0
        self.time = 0
        self.frame_rate = 60
        self.level = 0
        self.lives = LIVES
        self.idleTimer = 0
        self.activeTimer = 0
        self.displayedLevel = 0
        self.maxLevels = 2
        self.ghostScore = 5
        
        #Data to be passed to pacman
        self.score = 0
        self.internalScore = 0
        self.ttime = 0 #elapsed time for player
        self.neural_net = nn
        
    
    def setBackground(self):
        self.background = pygame.surface.Surface(SCREENSIZE).convert()
        self.background.fill(BLACK)

    def startGame(self):
        self.nodes = NodeGroup(self.level)
        self.pellets = PelletGroup(self.level)
        self.score = 0
        self.internalScore = 0
        self.frame_count = 0
        self.commonSetup()
    
    def restartLevel(self):
        self.commonSetup()
    
    def commonSetup(self):
        self.pacman = Pacman(self.nodes, self.level, self.neural_net)
        self.ghosts = None
        self.paused = False
        self.pauseTimer = 0
        self.pauseTime = 0
        self.playerPaused = False
        self.startDelay = False
        self.restartDelay = False
        self.scoreAccumulator = 0
        self.maxLives = MAXLIVES
    
    def play(self):
        self.startGame()
        while True:
            self.update()
            if not self.pacman.alive:
                return

    def applyScore(self):
        if self.scoreAccumulator > 0:
            newScore = self.score + self.scoreAccumulator
            if ((newScore % 10000 - self.score % 10000) < 0 or newScore - self.score >= 10000):
                if self.lives < self.maxLives:
                    self.lives += 1
            self.score += self.scoreAccumulator
            self.scoreAccumulator = 0
            self.score = newScore
    
    def updateTime(self):
        total_sec = self.frame_count // self.frame_rate
        minutes = total_sec // 60
        seconds = total_sec % 60
        return total_sec
    
    def checkEvents(self):
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                exit()
            if event.type == KEYDOWN:
                if event.key == K_SPACE:
                    if self.paused:
                        self.playerPaused = False
                    else:
                        self.playerPaused = True
                        print("Paused")
                    self.paused = not self.paused
                if event.key == K_ESCAPE:
                    pygame.quit()
                    exit()
    
    def checkPelletEvents(self, dt):
        pellet = self.pacman.eatPellets(self.pellets.pelletList)
        if pellet:
            self.idleTimer = 0
            self.scoreAccumulator += pellet.value
            self.pellets.numEaten += 1
            self.pellets.pelletList.remove(pellet)
            if self.pellets.isEmpty():
                self.pacman.alive = False
                self.paused = True
                self.pauseTime = 3
                self.pauseTimer = 0
                self.startDelay = True
        else:
            self.idleTimer += dt
    
    def kill(self):
        self.lives -= 1
        self.paused = True
        self.pauseTime = 0.1
        self.pauseTimer = 0
        self.pacman.alive = False
        if self.lives == 0:
            self.lives = LIVES
            self.startDelay = True
    
    def killTimer(self): #used during training; kills pacman if he's been idle for too long
        idleTime = round(self.idleTimer)
        if self.pacman.direction == STOP and idleTime != 0:
            if self.score < 100 and idleTime == 4:
                self.kill()
            elif self.score > 100 and idleTime == 25:
                self.kill()
            elif self.score > 200 and idleTime == 30:
                self.kill()
            elif self.score > 230 and idleTime == 35:
                self.kill()
        
        if self.score < 100 and idleTime == 3:
            self.kill()
        elif self.score > 100 and idleTime == 25:
            self.kill()
        elif self.score > 200 and idleTime == 30:
            self.kill()
        elif self.score > 230 and idleTime == 35:
            self.kill()
        
        if idleTime == 40:
            self.kill()
    
    def active(self, dt):
        idleTime = round(self.idleTimer)
        if self.pacman.direction != STOP and idleTime == 0:
            self.activeTimer += dt
            self.internalScore += dt
        else:
            self.activeTimer = 0

    def update(self):
        self.frame_count += 1
        self.time += 1
        dt = self.clock.tick(self.frame_rate) / 1000.0
        if not self.paused:
            self.pacman.update(dt, self.ghosts, self.pellets.pelletList)
            self.active(dt)
            self.checkPelletEvents(dt)
            self.killTimer()
        else:
            if not self.playerPaused:
                if not self.pacman.alive:
                    self.pacman.update(dt, self.ghosts, self.pellets.pelletList)
                    if self.startDelay == True:
                        self.play()
                    if self.restartDelay == True:
                        self.restartLevel()
                else:
                    self.pauseTimer += dt
                    if self.pauseTimer >= self.pauseTime:
                        self.paused = False
                    if self.startDelay == True:
                        self.startGame()
                    if self.restartDelay == True:
                        self.restartLevel()
        self.checkEvents()
        self.applyScore()
        self.ttime = self.updateTime()
        self.render()
    
    def render(self):
        self.screen.blit(self.background, (0, 0))
        self.nodes.render(self.screen)
        self.pellets.render(self.screen)
        self.pacman.render(self.screen)
        pygame.display.update()

""" if __name__ == "__main__":
    game = GameControl()
    game.startGame()
    while True:
        game.update() """