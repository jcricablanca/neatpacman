# NEATPacMan
A Pac-Man game written in Python 3 with an AI controlling Pac-Man in the maze that is able to complete the maze. The AI is implemented using the NEAT (NeuroEvolution of Augmenting Topologies) genetic algorithm.

## Setup 
This machine learning project uses Python 3 as the programming language and requires Pygame and NEAT-Python. It is best to use pip3 when installing packages. 
 
### Installing Pygame 
To install pygame, use: 
    
    
    pip3 install pygame

### Installing NEAT-Python 
To install NEAT-Python use: 
    
    
    pip3 install neat-python
    
### Installing other packages this project uses 
Installing graphviz, matplotlib, numpy 
    
    
    pip3 install graphviz 
    pip3 install matplotlib 
    pip3 install numpy

##Undergraduate Special Problem
* Link: http://tiny.cc/sp-jcricablanca

## Authors
* **John Carlo G. Ricablanca** - (jcg.ricablanca@gmail.com) - researcher
* **Juan Miguel J. Bawagan III** - research adviser